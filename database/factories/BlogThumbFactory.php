<?php

namespace Database\Factories;

use App\Models\Blog;
use App\Models\BlogThumb;
use App\Models\FileUpload;
use Illuminate\Database\Eloquent\Factories\Factory;

class BlogThumbFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = BlogThumb::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
