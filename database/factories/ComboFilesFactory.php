<?php

namespace Database\Factories;

use App\Models\ComboFiles;
use Illuminate\Database\Eloquent\Factories\Factory;

class ComboFilesFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ComboFiles::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
