<?php

namespace Database\Factories;

use App\Models\MovieFiles;
use Illuminate\Database\Eloquent\Factories\Factory;

class MovieFilesFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = MovieFiles::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
