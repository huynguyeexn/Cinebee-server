<?php

namespace Database\Factories;

use App\Models\AgeRating;
use Illuminate\Database\Eloquent\Factories\Factory;

class AgeRatingFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = AgeRating::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
