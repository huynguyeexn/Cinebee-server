<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableMovie extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('movies', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('slug');
            $table->string('trailer')->nullable();
            $table->string('thumbnail')->nullable();
            $table->integer('likes')->default(0);
            $table->text('description')->nullable();
            $table->dateTime('release_date')->nullable();
            $table->integer('running_time')->nullable();
            $table->foreignId('age_rating_id')
                ->nullable()
                ->constrained()
                ->cascadeOnUpdate();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('movie');
    }
}
