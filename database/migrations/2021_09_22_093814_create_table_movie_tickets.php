<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableMovieTickets extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('movie_tickets', function (Blueprint $table) {
            $table->id();
            $table->dateTime('get_at');
            $table->foreignId('showtime_id')
                ->constrained()
                ->cascadeOnUpdate();
            $table->foreignId('seat_id')
                ->constrained()
                ->cascadeOnUpdate();
            $table->float('price');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('movie_tickets');
    }
}
