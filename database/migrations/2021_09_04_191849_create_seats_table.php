<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSeatsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('seats', function (Blueprint $table) {
            $table->id();
            $table->string("name", 50);
            $table->unsignedTinyInteger("row");
            $table->unsignedTinyInteger("col");
            $table->unsignedTinyInteger("seat_status_id")->nullable();

            $table
                ->foreignId("room_id")
                ->nullable()
                ->constrained()
                ->cascadeOnUpdate();


            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('seats');
    }
}
