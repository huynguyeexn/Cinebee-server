<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateSeatsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('seats', function (Blueprint $table) {
            //
            $table->renameColumn("name", "label");
            $table->string("row")->change();
            $table->renameColumn("row", "index");
            $table->dropColumn("col");

            $table->foreignId("customer_id")
                ->nullable()
                ->constrained()
                ->cascadeOnUpdate();

            $table->string('customer_username')->nullable();
            $table->foreign('customer_username')
                ->references('username')
                ->on('customers')
                ->cascadeOnUpdate();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('seats', function (Blueprint $table) {
            //
        });
    }
}
