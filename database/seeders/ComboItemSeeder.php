<?php

namespace Database\Seeders;

use App\Models\ComboItem;
use Exception;
use Illuminate\Database\Seeder;

class ComboItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        try {
            ComboItem::factory(2)->create();
        } catch (Exception $e) {

            if ($this->failures > 5) {
                print_r(" Seeder Error. Failure count for current entity: " . $this->failures);
                return;
            }

            $this->failures++;
            $this->run(); // retry again until the number of failure is greater than 5
        }
    }
}
