FROM php:8-fpm
RUN apt-get update && apt-get install -y \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libpng-dev \
    && docker-php-ext-configure gd --with-freetype --with-jpeg \
    && docker-php-ext-install -j$(nproc) gd 
    RUN apt-get update \
     && apt-get install -y libzip-dev \
     && docker-php-ext-install zip


RUN docker-php-ext-install pdo pdo_mysql

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
RUN sed -E -i -e 's/post_max_size = 8M/post_max_size = 1G/' /usr/local/etc/php/php.ini-development \
    && sed -E -i -e 's/upload_max_filesize = 2M/upload_max_filesize = 1G/' /usr/local/etc/php/php.ini-development \
    && sed -E -i -e 's/post_max_size = 8M/post_max_size = 1G/' /usr/local/etc/php/php.ini-production \
    && sed -E -i -e 's/upload_max_filesize = 2M/upload_max_filesize = 1G/' /usr/local/etc/php/php.ini-production
# Use the default production configuration
RUN mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini"
# Set working directory
WORKDIR /var/www/html

# Install PHP Composer


# Copy existing application directory
COPY . .

