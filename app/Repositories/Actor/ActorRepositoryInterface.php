<?php
// long add 06-09-2021
namespace App\Repositories\Actor;

use App\Repositories\RepositoryInterface;

interface ActorRepositoryInterface extends RepositoryInterface
{
    public function getMovies($id);
}
