<?php
// long add 06-09-2021
namespace App\Repositories\Genre;

use App\Repositories\RepositoryInterface;


interface GenreRepositoryInterface extends RepositoryInterface
{
    public function getMovies($id);
}
